package net.tngou.jtdb.example;

import net.tngou.jtdb.Field;
import net.tngou.jtdb.Field.Type;
import net.tngou.jtdb.Fields;
import net.tngou.jtdb.Page;
import net.tngou.jtdb.SortField;
import net.tngou.jtdb.SortField.Order;
import net.tngou.jtdb.TngouDBHelp;
import net.tngou.jtdb.netty.TngouClient;

public class Select {

	public static void main(String[] args) {
		
		TngouDBHelp dbHelp = TngouDBHelp.getConnection();  //建立连接
		
		SortField sortField = new SortField("id", Order.ASC); //排序
		Field[] fields = new Field[1];
		fields[0]= new Field("id", "1");
		
		Page page = dbHelp.select("topword", sortField , 1, 10 );
		System.err.println(page);
		dbHelp.closeConnection();
		
		
		
		
		dbHelp.close();
	}

}
