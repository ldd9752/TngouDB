package net.tngou.jtdb.example;

import net.tngou.jtdb.Field;
import net.tngou.jtdb.Field.Type;
import net.tngou.jtdb.Fields;
import net.tngou.jtdb.TngouDBHelp;
import net.tngou.jtdb.netty.TngouClient;

public class Insert {

	public static void main(String[] args) {
		
		TngouDBHelp dbHelp = TngouDBHelp.getConnection();  //建立连接
		Field[] fields = new Field[3];
		fields[0] = new Field("id", "1", Type.Key);
		fields[1] = new Field("title", "中文索引数据库TngouDB", Type.Text);
		fields[2] = new Field("tclass", "1", Type.String);
		dbHelp.insert("topword", fields ); //插入数据
		dbHelp.closeConnection();
		
		
//		Fields fields1 = new Fields();
//		Field field = new Field("id", "2", Type.Key);
//		fields1.add(field);
//		field = new Field("title", "中文索引数据库TngouDB", Type.Text);
//		fields1.add(field);
//		field = new Field("tclass", "2", Type.String);
//		fields1.add(field);
//		dbHelp.insert("tngou", fields1 ); //插入数据
		dbHelp.closeConnection();

		dbHelp.close();
	}

	
	
}
