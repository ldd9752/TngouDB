TngouDB NoSQL全文搜索数据库
===============

Tngou 全文搜索数据库

采用网络存储结构

1： 数据存储Lucene 
2：网络通讯框架 netty

由于大量的缓存读取会导致 L2 的网络成为整个系统的瓶颈，因此 L1 的目标是降低对 L2 的读取次数



运行时所需 jar 包：

1. lib/commons-beanutils-1.8.2.jar  
3. lib/commons-logging-1.1.1.jar  
4. lib/commons-pool-1.6.jar  
5. lib/ehcache-2.7.5.jar  
6. lib/jedis-2.2.1.jar  
7. lib/jgroups-3.4.0.Final.jar  
8. lib/slf4j-*.jar
9. lib/fst-1.36.jar

您可以使用ant和maven俩种方式构建项目。