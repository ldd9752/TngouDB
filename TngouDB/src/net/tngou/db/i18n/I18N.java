package net.tngou.db.i18n;

import java.io.File;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.StringUtils;

import net.tngou.db.lucene.Config;

public class I18N {
	static PropertiesConfiguration config=null;
	
	static
	{
	
		try {
			String i18n= Config.getInstance().getLocale()+".properties";
			File propertiesFile = new File(i18n);
			Configurations configs = new Configurations();
			config= configs.properties(propertiesFile);
		
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String getValue(String name) {
		return config.getString(name);
	}
	
	public static String getValue(String name,String ...strings) {
		
		String msg = getValue(name);
		for (String string : strings) {
			msg=StringUtils.replace(msg, "{}", string, 1);
		}
		return msg;
		
	}
	
	
}
