package net.tngou.db.netty;


import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.tngou.db.i18n.I18N;
import net.tngou.db.lucene.Config;

/**
 * 
* @ClassName: Daemons
* @Description: TODO 后台程序运行监听程序，
* @author tngou@tngou.net (www.tngou.net)
* @date 2015年5月20日 下午2:34:15
*
 */
public class Daemons  extends Netty implements Runnable {
	private static Logger log = LoggerFactory.getLogger(Daemons.class);
	@Override
	public void run() {
		log.error(I18N.getValue("time_show","6s"));
		while (true) {			
			try {
				 TimeUnit.MILLISECONDS.sleep(6000);//6s的心跳检查
				 
				 int run =  Config.getInstance().getRun();
				 if(run==0)
				 { 
					 stop();//关闭
					 break;
				 }
					 
			} catch (InterruptedException e) {
				log.error(I18N.getValue("time_error"));
				e.printStackTrace();
			}
		}
		
		Thread.yield();
		

	}

}
